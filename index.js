const resquestURL = "http://localhost/api/index.php";

const hacerPeticion = () => {
  let entrada = document.getElementById("entrada").value;
  const header = document.getElementById('resultado');
  const request = new XMLHttpRequest();
  const newUrl = `${resquestURL}?codigo=${entrada}`;
  request.open('GET', newUrl, true);
  request.responseType = 'json';
  request.send();
  header.innerHTML = "";

  request.onload = async() => {
    const producto = await request.response; // json de respuesta
    populateHeader(producto); 
    showProducts(producto);
  }

  // Poblando el encabezado
  const populateHeader = (jsonObj) => {
    if(jsonObj['mensaje']) {
      const esError = document.createElement('h1');
      esError.textContent = "No se Encontró Producto";
      header.appendChild(esError);
    } else {
      const myH1 = document.createElement('h1');
      myH1.textContent = `Nombre: ${jsonObj['nombre']}`;
      header.appendChild(myH1);
      const myH2 = document.createElement('h2');
      let dollarUS = Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "USD",
      });
      myH2.textContent = `Precio: ${dollarUS.format(jsonObj['precio'])} MXN`;
      header.appendChild(myH2);
      const pic1 = document.createElement('img')
      pic1.src = `./img/${jsonObj['imagen']}`;
      pic1.setAttribute('width', '200px');
      header.appendChild(pic1);
    }
  }

  const showProducts = (jsonObj) => {
    const products = jsonObj['nombre'];
  }
}